<?php
require_once 'PHP/CompatInfo.php';
 
$dataSource = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'alkemy.php';
 
$info = new PHP_CompatInfo();
$info->parseFile($dataSource);
// you may also use unified method: $info->parseData($dataSource);
?>
