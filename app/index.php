<?php
/*
 * Copyright (c) 2011 Jeff Minard, http://jrm.cc
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

require 'libs/clicolors.php';

function out($msg) {
	static $STDOUT;
	if (!isset($STDOUT)) {
		$STDOUT = fopen('php://stdout', 'w');
	}
	$extra = implode('; ', array_splice(func_get_args(), 1));
	fwrite($STDOUT,
		   '['. clic::color(date('Y-m-d H:i:s'), 'light_green') .'] '
		   . clic::color(print_r($msg,1), 'yellow')
		   . (empty($extra) ? '' : "; $extra")
		   . "\n");
}

function err($msg) {
	static $STDERR;
	if (!isset($STDERR)) {
		$STDERR = fopen('php://stderr', 'w');
	}
	$extra = implode('; ', array_splice(func_get_args(), 1));
	fwrite($STDERR,
		   '['. clic::color(date('Y-m-d H:i:s'), 'light_green') .'] '
		   . clic::color(print_r($msg,1), 'red')
		   . (empty($extra) ? '' : "; $extra")
		   . "\n");
}

//out (__FILE__);
//out (__DIR__); // when run from the phar, not what you expect
//out (getcwd()); // when run from the phar, this is what you want
//out (dirname($argv[0]));
//out (print_r($argv,1));
//exit;

$version = 0.3;
$base = getcwd();
define('__PHARBASE__', __DIR__);
$command = isset($argv[1]) ? $argv[1] : false;

require 'libs/template.php';

require 'libs/json.minify.php';
$settings = (object)array();
if (file_exists('config/config.json')) {
	$json = file_get_contents('config/config.json');
	$settings = json_decode(json_minify($json));
}

switch ($command) {

	case 'init':
		out("Creating folder structure");

		$folderset = array(
			  'content'
			, 'content/assets'
			, 'content/images'
			, 'content/pages'
			, 'content/posts'
			, 'content/views'
			, 'site'
			, 'config'
		);

		foreach ($folderset as $folder) {
			if (!is_dir($folder)) {
				if (mkdir($folder)) {
					out('Created', $folder);
				}
				else {
					err('Could not create directory', $folder);
				}
			}
			else {
				out('Already exists', $folder);
			}
		}

		break;

	case 'new':
	case 'edit':

		if (!isset($argv[2]) || empty($argv[2]) || !($postsafe = toAscii($argv[2])) || empty($postsafe)) {
			err('Please provide a post name');
			break;
		}
		$posttitle = $argv[2];

		$filepathname = $base . '/content/posts/' . date('Y-m-d') . '-' . $postsafe . '.textile';

		if (!file_exists($filepathname)) {
			if (!is_dir(dirname($filepathname))) {
				out('Creating dir', dirname($filepathname));
				mkdir(dirname($filepathname), 0777, true);
			}

			touch($filepathname);
			file_put_contents($filepathname, "title: $posttitle\nview: \ntags: \nid: \n---\n\n");
		}

		system("nano -w $filepathname > `tty`");
		break;

	case 'assets':
		require 'libs/main_assets.php';
		main_assets();
		break;

	case 'update':
	case 'up':
		out('Starting update');

		// pre-process all content
		require 'libs/textile.php';
		$textile = new Textile;

		require 'libs/markdown.php';
		$markdown = new Markdown_Parser;

		$contentIndexDefaults = array(
			  'all' => array()
			, 'view' => array()
			, 'tags' => array()
			, 'series' => array()
			, 'viewtags' => array()
			, 'tagsinview' => array()
		);
		$assets = array('js'=>array(),'css'=>array());
		$contentIndex =
		$contentItems =
		$viewsNeeded =
		$bodyparts =
			array();

		if (!is_dir($base.'/content')) {
			err('No content directory exists. Perhaps you need to "init" the project first?');
			die;
		}

		require 'libs/main_assets.php';
		main_assets();

		// Deal with post and pages
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($base.'/content'), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $file => $cur){
			/** @var $cur RecursiveDirectoryIterator */
			/** @var $objects RecursiveDirectoryIterator */
			if (!$cur->isFile()) {
				continue;
			}

			$contentItem = array();

			$parts = explode('/',  $objects->getSubPath(), 2);
			if (count($parts) > 1) {
				list($type, $series) = $parts;
			}
			else {
				$type = $parts[0];
				$series = null;
			}

			$filename = $cur->getFilename();
			$extension = substr(strrchr($filename, '.'), 1);

			if ($type != 'posts' && $type != 'pages') {
				continue;
			}

			if (!isset($contentIndex[$type])) {
				$contentIndex[$type] = $contentIndexDefaults;
			}

			out('Parsing', $cur->getPathname());

			ob_start();
			require $cur->getPathname();
			$content = ob_get_clean();

			$content = str_replace(array("\r\n","\r"), "\n", $content);
			$parts = explode("\n---\n", $content, 2);

			if (count($parts) == 2) {
				list($header,$body) = $parts;
			}
			else {
				$header = false;
				$body = $parts[0];
			}

			if ($header !== false) {
				$contentLines = explode("\n", $header);
				foreach ($contentLines as $ln => $line) {
					$headerparts = explode(':', $line, 2);
					if (count($headerparts) != 2) {
						out('WARNING: Invalid header (file:line):', $cur->getPathname(), $line);
						continue;
					}
					list($key, $val) = $headerparts;
					$contentItem[strtolower(trim($key))] = trim($val);
				}
			}

			// don't parse items marked as skip
			if (array_key_exists('skip', $contentItem) !== false) {
				out('Skipped', $cur->getPathname());
				continue;
			}

			$contentItem['series'] = $series;

			// generate the body content (raw and parsed)
			$contentItem['contentraw'] = $body;

			// parse the content through textile/markdown/etc
			if ($extension == 'txt') {
				$body = '<p>'.nl2br($body).'</p>';
			}
			elseif ($extension == 'textile') {
				$body = $textile->TextileThis($body);
			}
			elseif ($extension == 'markdown') {
				$body = $markdown->transform($body);
			}

			$contentItem['content'] = $body;



			// defaults, in case they are not set
			if (!isset($contentItem['title']) || $contentItem['title'] == '') {
				$contentItem['title'] = preg_replace('/\d\d\d\d-\d\d-\d\d-/', '', preg_replace('/\.(txt|textile|html|markdown)/', '', $cur->getFilename()));
			}

			// make the title output safe, so you can skip this in the templates
			$contentItem['title'] = htmlspecialchars($contentItem['title']);

			if (!isset($contentItem['link'])) {
				// this, right here, would drive a java programmer insane
				$filepathname = $cur->getPathname();
				// extract date
				$filepathname = preg_replace('/\d\d\d\d-\d\d-\d\d-/', '', $filepathname);
				$filepathname = explode('.', $filepathname);
				array_pop($filepathname);
				$filepathname = implode('.', $filepathname);
				$filepathname = str_replace(array('content/posts','content/pages'), array('site', 'site'), $filepathname);// . '/index.html';

				$fname = preg_replace('/\.(txt|textile|html|markdown)/', '', $cur->getFilename());
				if ($fname == 'index') {
					$contentItem['link'] = preg_replace('/index$/', '', $filepathname);
					$filepathname .= '.html';
				}
				else {
					$contentItem['link'] = $filepathname;
					$filepathname .= '/index.html';
				}
				$contentItem['outfile'] = $filepathname;

				// clean up the link
				$halves = explode('/site/', $contentItem['link'], 2);
				$contentItem['link'] = '/' . trim($halves[1],'/') . '/';
			}
			else {
				if (!isset($contentItem['outfile'])) {
					err('If you set the link front matter, you must also set the outfile frontmatter');
					die;
				}
				$contentItem['outfile'] = $base . '/site/' . ltrim($contentItem['outfile'],'/');
			}



			if (!isset($contentItem['view'])) {
				$contentItem['view'] = null; //'default-view.php';
			}
			else {
				if (isset($viewsNeeded[$contentItem['view']])) {
					$viewsNeeded[$contentItem['view']] += 1 ;
				}
				else {
					$viewsNeeded[$contentItem['view']] = 1;
				}
			}

			if (!isset($contentItem['tags'])) {
				$contentItem['tags'] = array();
			}
			else {
				$taglist = explode(',', $contentItem['tags']);
				$cleanTags = array();
				foreach ($taglist as $tag) {
					$cleanTags[] = trim($tag);
				}
				$contentItem['tags'] = array_unique($cleanTags);
			}

			if (!isset($contentItem['summary'])) {
				$contentItem['summary'] = excerpt($contentItem['content']);
			}

			if (!isset($contentItem['date'])) {
				if ($filepathname = preg_match('/\d\d\d\d-\d\d-\d\d/', $cur->getPathname(), $m)){
					$contentItem['date'] = strtotime($m[0]);
				}
				else {
					$contentItem['date'] = filectime($cur->getPathname());
				}
			}
			else {
				$contentItem['date'] = strtotime($contentItem['date']);
			}

			$contentItem['file'] = $cur->getPathname();
			$contentItem['filename'] = $cur->getFilename();
			$contentItem['rdate'] = date('Y-m-d', $contentItem['date']);

			$contentItems[$type][$cur->getPathname()] = $contentItem;

		}

		// sort posts by date
		if (isset($contentItems['posts']) && count($contentItems['posts'])) {
			$dates = array();
			foreach ($contentItems['posts'] as $item) {
				$dates[] = $item['date'];
			}
			array_multisort($dates, $contentItems['posts']);
		}

//		out($contentItems); die;

		// Ensure that there are no link conflicts
		$linkList = array();
		$linkConflict = false;
		foreach ($contentItems as $type => $contentItemList) {
			foreach ($contentItemList as $id => $item) {

				if (!isset($linkList[$item['link']])) {
					$linkList[$item['link']] = $id;
				}
				else {
					out('Link conflict', $linkList[$item['link']], $id);
					$linkConflict = true;
				}

				$viewList[$item['view']] = 1;

			}
		}
		if ($linkConflict == true) {
			err('Two items are trying to create content in the same link path. Please retitle one of the articles so they do not overwrite one another.');
			exit;
		}


		// Ensure that all required views exist. Note: if a template makes a render call, those views are not checked until render time
		$viewMissing = false;
		foreach ($viewsNeeded as $view => $uses) {
			$viewpath = 'content/views/' . $view . '.php';
			if (!file_exists($viewpath)) {
				err('View missing', $viewpath);
				$viewMissing = true;
			}
		}
		if ($viewMissing == true) {
			err('One ore more required views is missing.');
			exit;
		}

		// cleanup checks
		unset($linkList, $viewsNeeded, $linkConflict, $viewMissing);



		// Create an index of content items by tag
		foreach ($contentItems as $type => $contentItemList) {
			foreach ($contentItemList as $id => $item) {

				// all items should have the tag "all"
				if ($type == 'posts') {
					if (count($item['tags'])) {
						if (!array_search('all', $item['tags'])) {
							$item['tags'][] = 'all';
						}
					}
					else {
						$item['tags'][] = 'all';
					}
				}

				foreach ($item['tags'] as $tag) {
					if (!isset($contentIndex[$type]['tags'][$tag])) {
						$contentIndex[$type]['tags'][$tag] = array();
					}
					$contentIndex[$type]['tags'][$tag][] = $id;

				}

				$contentIndex[$type]['all'][] = $id;

			}
		}


		// generate tag pages (also leaves the "tagpath" around
		if (isset($settings->tagPath) && isset($settings->postsPerTagPage)) {
			$tagpath = $settings->tagPath;
			if (substr($tagpath, -5) != '.html') {
				$tagpath = rtrim($tagpath, '/') . '/';
				$tagpathAdd = 'index.html';
			}
			else {
				$tagpathAdd = '';
			}
			$tagpath = '/' . ltrim($tagpath, '/');

			$perpage = $settings->postsPerTagPage;
			foreach ($contentItems as $type => $contentItemSet) {
				foreach ($contentIndex[$type]['tags'] as $tag => $postList) {

					$curpage = 0;
					$safeTag = toAscii($tag);

					// this is usually the order you want
					$postList = array_reverse($postList);

					while( $curPagePosts = array_splice($postList, 0, $perpage) ) {
						$curpage++;

						$outfile = $base . '/site' . sprintf($tagpath, $safeTag, $curpage) . $tagpathAdd;

						$rendered = renderView('tag', array(
							  'tag' => $tag
							, 'safetag' => $safeTag
							, 'posts' => $curPagePosts
							, 'nextPageLink' => count($postList) > 0 ? sprintf($tagpath, $safeTag, $curpage+1) : false
							, 'prevPageLink' =>         $curpage > 1 ? sprintf($tagpath, $safeTag, $curpage-1) : false
							, 'curPage' => $curpage
							, 'view' => 'tag'
						));

						// save it
						if (!is_dir(dirname($outfile))) {
							out('Creating dir', dirname($outfile));
							mkdir(dirname($outfile), 0777, true);
						}

						out('Creating tag page', $tag, $outfile);
						file_put_contents($outfile, $rendered);
					}

				}
			}
		}

//		out($contentIndex); die;

		// now for each content item, create the "site" version of it
		foreach ($contentItems as $type => $contentItemSet) {
			foreach ($contentItemSet as $filepath => $contentItem) {

				out('Compiling (src to site)', $filepath, $contentItem['outfile']);

				$rendered = renderView($contentItem['view'], array_merge($contentItem, array(
					'index' => $filepath
			  	)));

				// save it
				if (!is_dir(dirname($contentItem['outfile']))) {
					out('Creating dir', dirname($contentItem['outfile']));
					mkdir(dirname($contentItem['outfile']), 0777, true);
				}

				file_put_contents($contentItem['outfile'], $rendered);

			}
		}

		out('Updating complete');
		break;

	case 'clean':
		// clear out all contents?
		$toclear = $base.'/site';
		out('Cleaning out', $toclear);
		deleteInside($toclear);
		out('Done!');
		break;

	case 'license':
		out("Alkemy, version $version");
		out('Copyright (c) 2011 Jeff Minard, http://jrm.cc

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.');
		break;

	case 'info':
	case '?':
	case '-?':
	case 'help':
	case '--help':
		out("Alkemy, version $version");
		out("See the most recent documentation at http://alkemy.info/");
		out("Commands: (All operations assume you are working in the Alkemy site directory)");
		out("    init         Create a new Alkemy site");
		out("    up|update    (Re)Build the Alkemy site (also calls 'assets')");
		out("    new|edit     Create a new post (or edit an existing match)");
		out("    clean        Remove all file from the site folder");
		out("    assets       Rescan all assets and copy update the site folder with new versions");
		out("    license      View the software license");
		break;
		
	default:
		err('Unknown command -- try `alkemy help`');
		break;
		
}