<?php

function main_assets() {
	global $base, $assets;

	if (!is_dir($base.'/content')) {
		err('No content directory exists. Perhaps you need to "init" the project first?');
		die;
	}

	// Deal with post and pages
	$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($base.'/content'), RecursiveIteratorIterator::SELF_FIRST);
	foreach($objects as $file => $cur){
		/** @var $cur RecursiveDirectoryIterator */
		/** @var $objects RecursiveDirectoryIterator */
		if (!$cur->isFile()) {
			continue;
		}

		$contentItem = array();

		$parts = explode('/',  $objects->getSubPath(), 2);
		if (count($parts) > 1) {
			list($type, $series) = $parts;
		}
		else {
			$type = $parts[0];
			$series = null;
		}

		$filename = $cur->getFilename();
		$extension = substr(strrchr($filename, '.'), 1);

		if ($type == 'assets') {

			$filepathname = $cur->getPathname();
			$filepathname = str_replace('content/assets', 'site', $filepathname);

			if (file_exists($filepathname) && md5_file($cur->getPathname()) == md5_file($filepathname)) {
				out('Asset unchanged', $cur->getPathname());
			}
			else {
				if (!is_dir(dirname($filepathname))) {
					out('Creating dir', dirname($filepathname));
					mkdir(dirname($filepathname), 0777, true);
				}
				out('Copying asset', $cur->getPathname(), $filepathname);
				copy($cur->getPathname(), $filepathname);
			}

			$assets[$extension][] = $filename;

			// TODO: Combine files
//			if (isset($settings->combinecss) && $settings->combinecss) {
//			}

		}
	}
}
