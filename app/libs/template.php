<?php
/*
 * Copyright (c) 2011 Jeff Minard, http://jrm.cc
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Get a post contentItem for the given id
 * @param $id
 * @return array
 */
function getPost($id){
	return getContent($id, 'posts');
}

/**
 * Get a page contentItem for the given id
 * @param $id
 * @return array
 */
function getPage($id){
	return getContent($id, 'pages');
}

/**
 * Fetch the contentItem for the given id and type
 * @param string $id
 * @param string $type
 * @return array Item contents, empty if the item does not exist
 */
function getContent($id, $type) {
	global $contentItems;
	if (array_key_exists($type, $contentItems) && array_key_exists($id, $contentItems[$type])) {
		return $contentItems[$type][$id];
	}
	else {
		return array();
	}
}

/**
 * Find all post ids for a given a tag
 * @param string $tag
 * @return array
 */
function getPostsByTag($tag) {
	$list = getPostsTagList();
	if (array_key_exists($tag, $list)) {
		return $list[$tag];
	}
	else {
		return array();
	}
}

/**
 * Find all page ids for a given a tag
 * @param string $tag
 * @return array
 */
function getPagesByTag($tag) {
	$list = getPagesTagList();
	if (array_key_exists($tag, $list)) {
		return $list[$tag];
	}
	else {
		return array();
	}
}

/**
 * Get all tags belonging to posts
 * @return array
 */
function getPostsTagList() {
	return getContentList('posts', 'tags');
}

/**
 * Get all tags belonging to pages
 * @return array
 */
function getPagesTagList() {
	return getContentList('pages', 'tags');
}

/**
 * Based on the $contentIndex, fetch all items from the given contentType and listType
 * @param string $contenttype
 * @param string $listtype
 * @return array
 */
function getContentList($contenttype, $listtype) {
	global $contentIndex;
	if (array_key_exists($contenttype, $contentIndex) && array_key_exists($listtype, $contentIndex[$contenttype])) {
		return $contentIndex[$contenttype][$listtype];
	}
	else {
		return array();
	}
}

/**
 * Given a set of tags, find all posts which are in each of the given tags.
 * @param array $tags List of tags to cross reference
 * @return array An array of post ids (possibly empty)
 */
function getPostsInTags($tags) {
	$wrkArray = array();
	foreach ($tags as $tag) {
		$wrkArray[] = getPostsByTag($tag);
	}

	$result = call_user_func_array('array_intersect', $wrkArray);

	if ($result && count($result)) {
		return $result;
	}
	else {
		return array();
	}
}


/**
 * Recursively delete everything within a given directory
 * @param string $path The directory which should be cleaned
 * @return void
 */
function deleteInside($path) {
	if (!is_dir($path)) {
		err('Can not clean out path, not a dir',$path);
	}
    $it = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($path),
        RecursiveIteratorIterator::CHILD_FIRST
    );
    foreach ($it as $file) {
		/** @var $file RecursiveDirectoryIterator */
        if (in_array($file->getBasename(), array('.', '..'))) {
            continue;
        } elseif ($file->isDir()) {
            rmdir($file->getPathname());
        } elseif ($file->isFile() || $file->isLink()) {
            unlink($file->getPathname());
        }
    }
}


/**
 * Render the given view, passing in the parameters $v as an object
 * @param string $template
 * @param array $v An array of parameters to pass into the view as an object called $vars
 * @return string The view rendered into a string
 */
function renderView($template, $v=array()) {
	ob_start();
	if (empty($template)) {
		echo $v['content'];
	}
	else {
		$vars = (object)$v;
		require 'content/views/' . trim($template) . '.php';
	}
	return ob_get_clean();
}

/**
 * Find $imgName in the content/images folder, copy it to the site folder, apply any thumbnailing
 * routines ($params), and return the absolute path usable in a src attribute
 *
 * The params you may use mirror the params for use in TimThumb (http://www.binarymoon.co.uk/projects/timthumb/)
 *
 * I have also added 'au' which means "Allow Upsizeing" and is a boolean value (passing true/false or 0/1 will work).
 * Use the 'au' param if some of your images are NOT larger than the size you are shrinking to so that the image does
 * not get scaled up to the w/h parameters you set. (Unless you want that to happen, of course)
 *
 * @param string $imgName
 * @param array $params Specify options for how to treat the image at copy. See phpDoc for more info
 * @return mixed
 */
function image($imgName, $params=array()) {
	// check for the source
	// copy the source image into static files, (if it hasn't been already, check by md5)
	// if there are params, parse those and create a file to match (but only if the target hasn't md5 changed OR if the source changed)
	$source = 'content/images/' . $imgName;
	$destFull = 'site/images/' . $imgName;

	if (!is_file($destFull)) {
		// copy it over
		$destIsValid = false;
	}
	else {
		$destIsValid = md5_file($source) == md5_file($destFull);
	}

	if (!is_dir(dirname($destFull))) {
		out('Creating img dir', dirname($destFull));
		mkdir(dirname($destFull), 0777, true);
	}

	if (!$destIsValid) {
		// also copy it over
		out('img, copy only', $source, $destFull);
		copy($source, $destFull);
	}

	$imgtagsrc = $destFull;

	if (count($params)) {
		require_once __PHARBASE__.'/libs/phpthumb/ThumbLib.inc.php';
		require_once __PHARBASE__.'/libs/phpthumb/PhpThumb.inc.php';
		require_once __PHARBASE__.'/libs/phpthumb/ThumbBase.inc.php';
		require_once __PHARBASE__.'/libs/phpthumb/GdThumb.inc.php';
		// this image is supposed to be sized differently
		// figure out what the cached file name would be (based on the options)
		// If the file exists AND the source file HAS NOT changed, then the thumb need not be recreated.
		// If the file does not exist OR the source HAS changed, then recreate the thumbnail
		$fileToBe = imageCacheName($destFull, $params);

		if (file_exists($fileToBe) && $destIsValid) {
			// skip it, we're good.
		}
		else {
			// ah, either this thumb has never been made, or the source file changed, thus we need a refresh
			out('img, thumbnail', $destFull, $fileToBe);
			try
			{
				$thumb = PhpThumbFactory::create($destFull, $params);
				if (isset($params['w']) && (int)$params['w'] > 0 && isset($params['h']) && (int)$params['h'] > 0) {
					// adaptive
					$thumb
						->adaptiveResizeQuadrant($params['w'], $params['h'], (isset($params['c']) ? $params['c'] : null))
						->save($fileToBe);
				}
				elseif ((isset($params['w']) && (int)$params['w'] > 0) || (isset($params['h']) && (int)$params['h'] > 0)) {
					// resize to max of one
					$thumb
						->resize((isset($params['w']) ? (int)$params['w'] : 0), (isset($params['h']) ? (int)$params['h'] : 0))
						->save($fileToBe);
				}
				else {
					// You have to pick at least one resize value...
					err('You must select at least one dimension when resizing an image.', $destFull, json_encode($params));
					return '';
				}
			}
			catch (Exception $e) {
				err('Failed to open image.', $e->getMessage());
				return '';
			}
		}

		$imgtagsrc = $fileToBe;

	}

	return str_replace('site/', '/', $imgtagsrc);

}

/**
 * Given a file name and an array of parameters, come up with a unique filename.
 * @param string $imgFile
 * @param array $params
 * @return string
 */
function imageCacheName($imgFile, $params) {
	$filename = basename($imgFile);
	$hash = md5(serialize($params) . $imgFile);
	return str_replace($filename, 'th_'.$hash.'_'.$filename, $imgFile);
}

/**
 * Fetch the most recent $limit posts
 * @param int $limit
 * @return array
 */
function recentPosts($limit=5){
	global $contentItems;
	$postList = array_keys($contentItems['posts']);
	$postList = array_splice($postList, -$limit);
	$postList = array_reverse($postList);

	$return = array();
	foreach ($postList as $id) {
		$return[] = $contentItems['posts'][$id];
	}

	return $return;
}


/**
 * Fetch the next post
 * @return array|null An array of post info, or null if there is no next post
 */
function nextPost(){
	$a = npPost(1);
	return isset($a[0]) ? $a[0] : null;
}

/**
 * Fetch the previous post
 * @return array|null An array of post info, or null if there is no previous post
 */
function previousPost(){
	$a = npPost(-1);
	return isset($a[0]) ? $a[0] : null;
}

/**
 * Fetch the next $limit posts
 * @param int $limit
 * @return array
 */
function nextPosts($limit=5){
	return npPost($limit);
}

/**
 * Fetch the previous $limit posts (you do not need to pass a negative number here)
 * @param int $limit
 * @return array
 */
function previousPosts($limit=5){
	return npPost(-$limit);
}

/**
 * Based on the current post being rendered, return the next/previous $num posts
 *
 * Optionally, if you specify the $by and $byOption values, you can return the next/previous $num posts within just that contentIndex type. This would typically be used to show the next/previous posts for a given tag, ie:
 *
 *  // Get the next post from the automotive tag list
 *  $nextPost = npPost(1, 'tags', 'Automotive');
 *
 * Useful if you wish to make special "by type" navigation structures instead of just simple "next/prev" setups.
 *
 * @param int $num
 * @param string $by Optional, "all" or "tags" only at this point
 * @param null $byOption Optional, if used on "tags" will show the next/previous post by the given tag name
 * @return array
 */
function npPost($num, $by='all', $byOption=null){
	global $filepath, $contentItems, $contentIndex;

	if ($num==0) {
		return array();
	}

	if ($by == 'all') {
		$postList = $contentIndex['posts']['all'];
	}
	else {
		$postList = $contentIndex['posts'][$by][$byOption];
	}

	if ($num < 0) {
		$postList = array_reverse($postList);
	}

	$cur = array_search($filepath, $postList);
	if($cur !== false) {
		$postIds = array_slice($postList, $cur+1, abs($num));
	}
	else {
		return array();
	}

	$postList = array();
	foreach ($postIds as $id) {
		$postList[] = $contentItems['posts'][$id];
	}

	return $postList;
}

//// DISABLED
///**
// * Render out all css and js tags and return a single string with the HTML snippets
// * @return string
// */
//function allassets() {
//	return allAssetType('css') . allAssetType('js');
//}
//
///**
// * Given an asset type, return all those assets rendered in their HTML string snippets
// * @param string $type "css" or "js"
// * @return string
// */
//function allAssetType($type) {
//	global $assets;
//	$out = array();
//	foreach ($assets[$type] as $file) {
//		if ($type == 'css') {
//			$out[] = cssfile($file);
//		}
//		elseif ($type == 'js') {
//			$out[] = jsfile($file);
//		}
//	}
//	return implode("\n", $out) . "\n";
//}
//
///**
// * Render out the HTML snippet for the <head> tag for a given CSS file
// * @param string $file
// * @return string
// */
//function cssfile($file) {
//	return '<link rel="stylesheet" href="/'. $file .'" >';
//}
//
///**
// * Render out the HTML snippet for the <head> tag for a given JS file
// * @param string $file
// * @return string
// */
//function jsfile($file) {
//	return '<script src="/'. $file .'"></script>';
//}

/**
 * Create an excerpt of a given string of text
 * @param string $text
 * @return string
 */
function excerpt($text) {
	$text = str_replace('\]\]\>', ']]&gt;', $text);
	$text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
	$text = strip_tags($text);
	$excerpt_length = 55;
	$words = explode(' ', $text, $excerpt_length + 1);
	if (count($words) > $excerpt_length) {
		array_pop($words);
		array_push($words, '[...]');
		$text = implode(' ', $words);
	}
	return $text;
}

/**
 * Return a url safe string
 * @param string $str
 * @param array $replace
 * @param string $delimiter
 * @return string
 */
function toAscii($str, $replace=array(), $delimiter='-') {
	setlocale(LC_ALL, 'en_US.UTF8');
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}

/**
 * Return the url link to a tag page for the given tag and page number
 *
 * You must have the tagpath setting configured properly.
 *
 * @param string $tagname
 * @param int $page
 * @return string
 */
function getTagLink($tagname, $page=1) {
	global $tagpath;
	if (!isset($tagpath) || empty($tagpath)) {
		err('No tagpath has been set. Check your config.js file.');
	}
	return sprintf($tagpath, toAscii($tagname), $page);
}

