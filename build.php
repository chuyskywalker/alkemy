<?php
/*
 * Copyright (c) 2011 Jeff Minard, http://jrm.cc
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

$build = __DIR__ . '/builds/alkemy.phar';

if (file_exists($build)) {
	unlink($build);
}

$p = new Phar($build, FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO, 'alkemy.phar');
$p->startBuffering();

$p->setStub('<?php
Phar::mapPhar();
/*Phar::interceptFileFuncs();*/
include "phar://alkemy.phar/index.php";
__HALT_COMPILER();
');

$p['index.php'] = file_get_contents(__DIR__ . '/app/index.php');

foreach (glob(__DIR__ . '/app/libs/*php') as $file) {
	$filename = basename($file);
	$p['libs/' . $filename] = file_get_contents($file);
}

foreach (glob(__DIR__ . '/app/libs/phpthumb/*php') as $file) {
	$filename = basename($file);
	$p['libs/phpthumb/' . $filename] = file_get_contents($file);
}

$p->stopBuffering();

//unset($p);
//
//// reopen to examine
//
//$p = new Phar($build, FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO, 'app.phar');
//// Phar extends SPL's DirectoryIterator class
//foreach (new RecursiveIteratorIterator($p) as $file) {
//	// $file is a PharFileInfo class, and inherits from SplFileInfo
//	echo $file->getPathName() . "\n";
//	//var_dump($file->getMetaData());
//	//var_dump($file->getPharFlags());
//	//echo file_get_contents($file->getPathName()) . "\n"; // display contents;
//}

$scriptbuild = __DIR__.'/builds/alkemy';
if (file_exists($scriptbuild)) {
	unlink($scriptbuild);
}
copy('./alkemy.sh', $scriptbuild);
chmod($scriptbuild, 0777);

$licensebuild = __DIR__.'/builds/LICENSE';
if (file_exists($licensebuild)) {
	unlink($licensebuild);
}
copy('./LICENSE', $licensebuild);

$buiddir = __DIR__.'/builds';
$tgzbuild = __DIR__.'/builds/alkemy.tgz';
$zipbuild = __DIR__.'/builds/alkemy.zip';

exec("cd $buiddir && tar -czf alkemy.tgz alkemy.phar alkemy LICENSE");
exec("cd $buiddir && zip -9 alkemy.zip alkemy.phar alkemy LICENSE");

copy($buiddir.'/alkemy.tgz', __DIR__.'/public/content/assets/downloads/alkemy.tgz');
copy($buiddir.'/alkemy.zip', __DIR__.'/public/content/assets/downloads/alkemy.zip');

