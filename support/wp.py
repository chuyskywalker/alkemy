import string, os, sys, getopt
from xml.dom import minidom

#import pprint

# Author :  Jain Basil Aliyas <jainbasil@gmail.com>
# Homepage : http://jainbasil.net


def convert(infile):
	"""
		Convert Wordpress Export File to multiple html files.
	"""
	
	
	# First we parse the XML file into a list of posts.
	# Each post is a dictionary
	
	dom = minidom.parse(infile)

	blog = [] # list that will contain all posts

	for node in dom.getElementsByTagName('item'):
		post = dict()

		postItems = {'title': 'title', 'id': 'wp:post_id', 'date': 'pubDate', 'postname': 'wp:post_name', 'postdate': 'wp:post_date', 'posttype': 'wp:post_type', 'text': 'content:encoded'};
		for key in postItems:
			if node.getElementsByTagName(postItems[key])[0].firstChild != None:
				post[key] = node.getElementsByTagName(postItems[key])[0].firstChild.data
			else:
				post[key] = ""
	
		print post['id']
	
		# clean this one up a small bit, gets just the date
		post["postdate"] =''.join(post["postdate"].split(' ')[0])
		
		# Get the categories and tags
		tempCategories = []
		tempTags = []
		for subnode in node.getElementsByTagName('category'):
			if subnode.getAttribute('domain') == "category":
				tempCategories.append(subnode.firstChild.data)
			if subnode.getAttribute('domain') == "tag":
				tempTags.append(subnode.firstChild.data)
	
		# I got all the tags twice in the list, so I am just removing the duplicates by following line. 
		# I don't know whether this is found in all the xml files.
		tempTags = list(set(tempTags))
	
		categories = [x for x in tempCategories if x != '']
		tags = [x for x in tempTags if x != '']
		
		post["categories"] = categories 
		post["tags"] = tags
		
		# meta items
		meta = node.getElementsByTagName('wp:postmeta');
		
		post['link'] = ''
		post['image'] = ''
		
		if len(meta):
			# link
			for subnode in node.getElementsByTagName('wp:postmeta'):
				if subnode.getElementsByTagName('wp:meta_key')[0].firstChild.data == 'url':
					post['link'] = subnode.getElementsByTagName('wp:meta_value')[0].firstChild.data
			# images
			for subnode in node.getElementsByTagName('wp:postmeta'):
				if subnode.getElementsByTagName('wp:meta_key')[0].firstChild.data == 'gal_image':
					post['image'] = subnode.getElementsByTagName('wp:meta_value')[0].firstChild.data

		# Add post to the list of all posts
		blog.append(post)
		
	
	# Then we create the directories and HTML files from the list of posts.
	
	for post in blog:
		if post["posttype"].encode('utf-8') == 'attachment': 
			continue

		# Jekyll recognize html file as yyyy-mm-dd-title-of-the-post.html
		title = post["title"].encode('utf-8')
		postname = post["postname"].encode('utf-8')
		postdate = post["postdate"].encode('utf-8')
		if postname == '':
			postname = post["id"]
		filename = postdate + '-' + postname + '.html'
	
		# Add a meta tag to specify charset (UTF-8) in the HTML file
		filename = 'content/posts/' + filename.replace('/',' ')
		f = open(filename, 'w')
		titletemp = title.replace(':',' ')
		
		layout = 'post'
		if len(post["categories"]) == 1:
			layout = post["categories"][0].encode('utf-8').lower()
			post["categories"] = []
		
		fileTitle = "title: " + titletemp + "\n" \
			+ "view: " + layout + "\n"  \
			+ "id: " + post["id"].encode('utf-8') + "\n"
		
		if post['link']:
			fileTitle += 'link: ' + post['link'] + "\n"
		
		if post['image']:
			fileTitle += 'image: ' + post['image'] + "\n"
		
		f.write(fileTitle)

		#Adding categories into file
		if post["categories"]:
			tempCats = []
			for c in post["categories"]:
				c = c.encode('utf-8')
				if c != '':
					tempCats.append(c)
			if len(tempCats):
				f.write("categories: " + ', '.join(tempCats) + "\n")

		# Adding Tags into file
		if post["tags"]:
			tempTags = []
			for c in post["tags"]:
				c = c.encode('utf-8')
				if c != '':
					tempTags.append(c)
			if len(tempTags):
				f.write("tags: " + ', '.join(tempTags) + "\n")

		f.write("---\n\n")
		
		# Convert the unicode object to a string that can be written to a file
		# with the proper encoding (UTF-8)
		text = post["text"].encode('utf-8')
		
		f.write(text)
		f.write("\n\n")
	
		f.close()

def main(argv):

	infile = sys.argv[1]
	if infile == "":
		print "Error: Missing Argument: missing wordpress export file."
		usage(argv[0])
		sys.exit(3)

	convert(infile)

if __name__ == "__main__":
	main(sys.argv)
