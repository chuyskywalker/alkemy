

		</div>

		<div id="documentation" class="sidebar">

			<p>Documentation</p>
			<ul>
<?php foreach (recentPosts(999) as $post) { ?>
				<li class="<?= $post['title'] == $vars->title ? 'current-page ' : '' ?>link-page-<?= toAscii($post['title']) ?>"><a href="<?= $post['link'] ?>"><?= $post['title'] ?></a></li>
<?php } ?>
			</ul>

		</div>

		<div id="download" class="sidebar">
			<p>Download</p>
			<ul>
				<li class="downloadlink <?= toAscii($vars->title) == 'download'  ? 'current-page ' : '' ?>"><a href="/download/">Download</a></li>
				<li class="<?= toAscii($vars->title) == 'changelog' ? 'current-page ' : '' ?>"><a href="/changelog/">Changelog</a></li>
			</ul>
		</div>

		<div class="clear"></div>

		<p class="copyright">&copy; <?= date('Y') ?> <a href="http://jrm.cc">Jeff Minard</a></p>
		
	</body>
</html>