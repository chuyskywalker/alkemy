<!DOCTYPE html>
<html>
	<head>
		<title><?= $vars->title ?> - Alkemy</title>
		<link rel="stylesheet" href="/site.css" >
	</head>
	<body class="page-<?= toAscii($vars->title) ?>">
	
		<div id="header">
			<h1><a href="/">Alkemy</a></h1>
			<p>Website Generation from Static Files</p>
		</div>
		
		<div class="clear"></div>

		<div class="content">


